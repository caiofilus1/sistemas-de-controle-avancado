# secord.py - demonstrate some standard MATLAB commands
# RMM, 25 May 09

import os
import matplotlib.pyplot as plt   # MATLAB plotting functions
from control.matlab import *  # MATLAB-like functions
from numpy import *  # MATLAB-like functions

[padeNum, padeDen] = pade(3, 1)
print(padeNum)
H = tf(padeNum, convolve(padeDen, [1, 8, 28, 56, 70, 56, 28, 8, 1]))
print(H)
s = step(H, 50)
print(s)
plt.plot(s[1], s[0])
plt.show()